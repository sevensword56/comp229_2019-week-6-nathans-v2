public class MoveLeft implements AI
{
    public void moveLoc(Actor a)
    {
        if((int) a.loc.col - a.moves > (int) 'A')
            a.loc.col = (char) ((int) a.loc.col - a.moves);
        else if ((int) a.loc.col - a.moves <= (int) 'A')
            a.loc.col = 'A';
    }
}