import java.util.*;

public class MoveRandom implements AI
{
    public void moveLoc(Actor a)
    {
        Random rand = new Random();

        int moveDirection = rand.nextInt(8);

        switch(moveDirection)
        {
            case 0:
                if(a.loc.row - a.moves > 0)
                    a.loc.row = a.loc.row - a.moves;
                else if(a.loc.row - a.moves <= 0)
                    a.loc.row = 0;
                break;

            case 1:
                if(a.loc.row - a.moves > 0)
                    a.loc.row = a.loc.row - a.moves;
                else if(a.loc.row - a.moves <= 0)
                    a.loc.row = 0;
                if((int) a.loc.col + a.moves < (int) 'T')
                    a.loc.col = (char) ((int) a.loc.col + a.moves);
                else if ((int) a.loc.col + a.moves >= (int) 'T')
                    a.loc.col = 'T';
                break;

            case 2:
                if((int) a.loc.col + a.moves < (int) 'T')
                    a.loc.col = (char) ((int) a.loc.col - a.moves);
                else if ((int) a.loc.col + a.moves >= (int) 'T')
                    a.loc.col = 'T';
                break;

            case 3:
                if(a.loc.row + a.moves < 19)
                    a.loc.row = a.loc.row + a.moves;
                else if(a.loc.row + a.moves >= 19)
                    a.loc.row = 19;
                if((int) a.loc.col + a.moves < (int) 'T')
                    a.loc.col = (char) ((int) a.loc.col + a.moves);
                else if ((int) a.loc.col + a.moves >= (int) 'T')
                    a.loc.col = 'T';
                break;

            case 4:
                if(a.loc.row + a.moves < 19)
                    a.loc.row = a.loc.row + a.moves;
                else if(a.loc.row - a.moves >= 19)
                    a.loc.row = 19;
                break;

            case 5:
                if(a.loc.row + a.moves < 19)
                    a.loc.row = a.loc.row + a.moves;
                else if(a.loc.row + a.moves >= 19)
                    a.loc.row = 19;
                if((int) a.loc.col - a.moves > (int) 'A')
                    a.loc.col = (char) ((int) a.loc.col + a.moves);
                else if ((int) a.loc.col + a.moves <= (int) 'A')
                    a.loc.col = 'A';
                break;

            case 6:
                if((int) a.loc.col - a.moves > (int) 'A')
                    a.loc.col = (char) ((int) a.loc.col - a.moves);
                else if ((int) a.loc.col - a.moves <= (int) 'A')
                    a.loc.col = 'A';
                break;

            case 7: 
                if(a.loc.row - a.moves > 0)
                    a.loc.row = a.loc.row - a.moves;
                else if(a.loc.row - a.moves <= 0)
                    a.loc.row = 0;

                if((int) a.loc.col - a.moves > (int) 'A')
                    a.loc.col = (char) ((int) a.loc.col - a.moves);
                else if ((int) a.loc.col - a.moves <= (int) 'A')
                    a.loc.col = 'A';
                break;
        }


    }
}